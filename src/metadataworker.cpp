/*
 * This file is a part of Xpiks - cross platform application for
 * keywording and uploading images for microstocks
 * Copyright (C) 2020-2021 Taras Kushnir <tk.dev@mailbox.org>
 */

#include "metadataworker.h"

#include <algorithm>
#include <functional>
#include <vector>

#include <QFile>
#include <QFileInfo>
#include <QJsonArray>
#include <QJsonObject>
#include <QtConcurrent>

#include "Helpers/constants.h"
#include "Helpers/exiv2iohelpers.h"
#include "Helpers/filehelpers.h"

const int PENDING_JOBS_WAIT_TIME = 50;
const int REMOVE_BACKUP_RETRIES = 4;
const int PARALLEL_PROCESSING_THRESHOLD = 50;

void setMetadataFromJson(const QJsonObject &object, BasicMetadata &metadata) {
    if (object.contains(TITLE_KEY)) {
        metadata.m_Title = object[TITLE_KEY].toString();
        Common::SetFlag(metadata.m_UseFlags, Common::MetadataFlags::UseTitle);
    }

    if (object.contains(DESCRIPTION_KEY)) {
        metadata.m_Description = object[DESCRIPTION_KEY].toString();
        Common::SetFlag(metadata.m_UseFlags, Common::MetadataFlags::UseDescription);
    }

    if (object.contains(COPYRIGHT_KEY)) {
        metadata.m_Copyright = object[COPYRIGHT_KEY].toString();
        Common::SetFlag(metadata.m_UseFlags, Common::MetadataFlags::UseCopyright);
    }

    if (object.contains(KEYWORDS_KEY)) {
        QJsonArray keywordsArray = object[KEYWORDS_KEY].toArray();
        metadata.m_Keywords.reserve(keywordsArray.size());

        for (const auto &keyword: keywordsArray) {
            if (keyword.isString()) {
                metadata.m_Keywords.append(keyword.toString());
            }
        }

        // we might actually want to write empty keywords
        // if (!metadata.m_Keywords.isEmpty()) {
        Common::SetFlag(metadata.m_UseFlags, Common::MetadataFlags::UseKeywords);
    }
}

bool validateFileRequest(const QString &filepath, QJsonObject &fileResponse) {
    if (filepath.isEmpty()) {
        LOG_WARNING << "Encountered an empty file path";
        fileResponse[ERROR_KEY] = "File name is missing or empty";
        return false;
    }

    if (!QFileInfo::exists(filepath)) {
        LOG_WARNING << "File doesn't exist:" << PII(filepath);
        fileResponse[ERROR_KEY] = "File doesn't exist";
        return false;
    }

    return true;
}

bool backupFile(const QString &filepath, QJsonObject &fileResponse) {
    const QString backupPath = filepath + "_original";

    if (QFileInfo::exists(backupPath) && !Helpers::removeFile(backupPath, REMOVE_BACKUP_RETRIES)) {
        LOG_WARNING << "Failed to remove previous backup copy of" << PII(filepath);
        fileResponse[ERROR_KEY] = "Couldn't remove previous backup copy";
        return false;
    }

    if (!QFile::copy(filepath, backupPath)) {
        LOG_WARNING << "Failed to make a backup copy of" << PII(filepath);
        fileResponse[ERROR_KEY] = "Couldn't make a backup copy";
        return false;
    }

    return true;
}

void restoreFile(const QString &filepath, QJsonObject &fileResponse) {
    const QString backupPath = filepath + "_original";

    QFile backup(backupPath);
    if (!backup.exists()) {
        LOG_WARNING << "Failed to find backup copy of" << PII(filepath);
        fileResponse[ERROR_KEY] = "Couldn't find backup copy";
        return;
    }

    if (!QFile::remove(filepath)) {
        LOG_WARNING << "Failed to remove" << PII(filepath);
        fileResponse[ERROR_KEY] = "Couldn't remove the file";
        return;
    }

    if (!backup.rename(filepath)) {
        LOG_WARNING << "Failed to rename backup copy of" << PII(filepath);
        if (!backup.copy(filepath)) {
            LOG_WARNING << "Failed to copy backup file" << PII(filepath);
            fileResponse[ERROR_KEY] = "Couldn't move nor copy backup file";
        } else {
            LOG_INFO << "Copied file instead of moving it" << PII(filepath);
        }
    }
}

void MetadataWorker::waitForJobs() {
    LOG_DEBUG << "#";
    const size_t MAX_JOBS_TO_WAIT = 2;
    size_t jobsToWait = std::max(retrievePendingCount(), MAX_JOBS_TO_WAIT);
    size_t prevCount = retrievePendingCount();
    size_t jobsDone = 0;
    while (hasPendingJobs()) {
        QThread::msleep(PENDING_JOBS_WAIT_TIME);
        const size_t count = retrievePendingCount();
        jobsDone += prevCount - count;
        if (jobsDone >= jobsToWait) {
            break;
        }
        prevCount = count;
    }
}

std::shared_ptr<QJsonDocument> MetadataWorker::processWorkItem(WorkItem &workItem) {
    const QString id = workItem.m_Item->value(ID_KEY).toString();
    LOG_DEBUG << "Processing ID:" << id;

    if (id.isEmpty()) {
        LOG_WARNING << "Empty request ID";
        return {};
    }

    auto response = std::make_shared<QJsonDocument>();
    const QJsonObject &request = *workItem.m_Item;
    QString type = workItem.m_Item->value(TYPE_KEY).toString();

    if (type == READ_TYPE) {
        handleRead(request, *response);
    } else if (type == WRITE_TYPE) {
        handleWrite(request, *response);
    } else if (type == WIPE_TYPE) {
        handleWipe(request, *response);
    } else if (type == RESTORE_TYPE) {
        handleRestore(request, *response);
    } else {
        LOG_WARNING << "Unknown request type:" << type;
        QJsonObject reply;
        reply[ID_KEY] = request[ID_KEY];
        reply[ERROR_KEY] = "Unknown request type: " + type;
        response->setObject(reply);
    }

    return response;
}

void MetadataWorker::onResultsAvailable(std::vector<WorkResult> &results) {
    LOG_DEBUG << results.size();
    for (const WorkResult &result: results) {
        QJsonDocument &response = *result.m_Result;
        emit responseAvailable(response);
    }
}

void MetadataWorker::onQueueIsEmpty() {
    LOG_DEBUG << "#";
    emit queueIsEmpty();
}

QJsonArray forEachArrayItem(const QJsonArray &array, const std::function<QJsonObject(const QJsonValue&)> &transform) {
    if (array.size() >= PARALLEL_PROCESSING_THRESHOLD) {
        return QtConcurrent::blockingMappedReduced(array, transform, &QJsonArray::append);
    }

    QJsonArray result;
    for (const QJsonValue &item: array) {
        result.append(transform(item));
    }
    return result;
}

QJsonObject doReadMetadata(const QJsonValue &value) {
    QString filepath = value.toString();

    BasicMetadata metadata;
    QJsonObject fileResponse;
    fileResponse.insert(SOURCE_FILE_KEY, filepath);

    do {
        if (!validateFileRequest(filepath, fileResponse)) { break; }

        if (!readMetadata(filepath, metadata)) {
            LOG_WARNING << "Failed to read metadata of" << PII(filepath);
            fileResponse[ERROR_KEY] = "Couldn't read metadata";
            break;
        }

        fileResponse.insert(TITLE_KEY, metadata.m_Title);
        fileResponse.insert(DESCRIPTION_KEY, metadata.m_Description);
        fileResponse.insert(COPYRIGHT_KEY, metadata.m_Copyright);
        QJsonArray keywords = keywords.fromStringList(metadata.m_Keywords);
        fileResponse.insert(KEYWORDS_KEY, keywords);
    } while (false);

    return fileResponse;
}

void MetadataWorker::handleRead(const QJsonObject &request, QJsonDocument &response) {
    LOG_DEBUG << "#";

    QJsonArray files = request[FILES_KEY].toArray();
    QJsonArray filesArray = forEachArrayItem(files, doReadMetadata);

    QJsonObject reply;
    reply[ID_KEY] = request[ID_KEY];
    reply[FILES_KEY] = filesArray;
    response.setObject(reply);
}

QJsonObject doWriteMetadata(const QJsonValue &value, bool withBackups) {
    QJsonObject file = value.toObject();
    const QString filepath = file[SOURCE_FILE_KEY].toString();

    QJsonObject fileResponse;
    fileResponse.insert(SOURCE_FILE_KEY, filepath);

    do {
        if (!validateFileRequest(filepath, fileResponse)) { break; }

        BasicMetadata metadata;
        setMetadataFromJson(file, metadata);

        if (withBackups && !backupFile(filepath, fileResponse)) { break; }

        if (!writeMetadata(filepath, metadata)) {
            LOG_WARNING << "Failed to update metadata of" << PII(filepath);
            fileResponse[ERROR_KEY] = "Couldn't update metadata";
        }
    } while (false);

    return fileResponse;
}

void MetadataWorker::handleWrite(const QJsonObject &request, QJsonDocument &response) {
    LOG_DEBUG << "#";

    const bool withBackups = request[BACKUPS_KEY].toBool();
    QJsonArray files       = request[FILES_KEY].toArray();

    auto transform = [withBackups](const QJsonValue &value) {
        return doWriteMetadata(value, withBackups);
    };
    QJsonArray filesArray = forEachArrayItem(files, transform);

    QJsonObject reply;
    reply[ID_KEY] = request[ID_KEY];
    reply[FILES_KEY] = filesArray;
    response.setObject(reply);
}

QJsonObject doWipeMetadata(const QJsonValue &value, bool withBackups) {
    const QString filepath = value.toString();

    QJsonObject fileResponse;
    fileResponse.insert(SOURCE_FILE_KEY, filepath);

    do {
        if (!validateFileRequest(filepath, fileResponse)) { break; }

        if (withBackups && !backupFile(filepath, fileResponse)) { break; }

        if (!wipeMetadata(filepath)) {
            LOG_WARNING << "Failed to wipe metadata of" << PII(filepath);
            fileResponse[ERROR_KEY] = "Couldn't wipe metadata";
        }
    } while (false);

    return fileResponse;
}

void MetadataWorker::handleWipe(const QJsonObject &request, QJsonDocument &response) {
    LOG_DEBUG << "#";

    const bool withBackups = request[BACKUPS_KEY].toBool();
    QJsonArray files       = request[FILES_KEY].toArray();

    auto transform = [withBackups](const QJsonValue &value) {
        return doWipeMetadata(value, withBackups);
    };
    QJsonArray filesArray = forEachArrayItem(files, transform);

    QJsonObject reply;
    reply[ID_KEY] = request[ID_KEY];
    reply[FILES_KEY] = filesArray;
    response.setObject(reply);
}

void MetadataWorker::handleRestore(const QJsonObject &request, QJsonDocument &response) {
    LOG_DEBUG << "#";

    QJsonArray files = request[FILES_KEY].toArray();
    QJsonArray filesArray;

    const int size = files.size();
    for (int i = 0; i < size; ++i) {
        QString filepath = files.at(i).toString();

        QJsonObject fileResponse;
        fileResponse.insert(SOURCE_FILE_KEY, filepath);

        do {
            if (!validateFileRequest(filepath, fileResponse)) { break; }

            restoreFile(filepath, fileResponse);
        } while (false);

        filesArray.append(fileResponse);
    }

    QJsonObject reply;
    reply[ID_KEY] = request[ID_KEY];
    reply[FILES_KEY] = filesArray;
    response.setObject(reply);
}
