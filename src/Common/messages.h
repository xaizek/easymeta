/*
 * This file is a part of EasyMeta - cross-platform application
 * for basic metadata editing in photos, vectors and videos
 * Copyright (C) 2020-2021 Taras Kushnir <tk.dev@mailbox.org>
 */

#ifndef CHANGESLISTENER_H
#define CHANGESLISTENER_H

#include <functional>
#include <initializer_list>
#include <vector>

#include "Common/logging.h"

namespace Common {
    // tag for events with same underlying type
    struct MessageType {
        enum Type {
            SpellCheck,
            EditingPaused,
            UnavailableFiles,
            VideoThumbnail,
            VectorThumbnail
        };
    };

    class IMessageBus {
    public:
        virtual ~IMessageBus() {}
        virtual bool IsMessagingEnabled() = 0;
    };

    template<typename T>
    class MessagesTarget {
    public:
        virtual ~MessagesTarget() {}
        virtual void handleMessage(const T &message) = 0;
    };

    template<typename T>
    class MessagesSource {
    public:
        virtual ~MessagesSource() {}

    public:
        void addTarget(MessagesTarget<T> &target) {
            m_Targets.push_back(target);
        }

        void sendMessage(T const &message) const {
            for (auto &target: m_Targets) {
                if (!IsEnabled()) { break; }
                target.get().handleMessage(message);
            }
            if (m_Targets.empty()) {
                LOG_WARNING << "Targets are empty" << REPORT_ME;
            }
        }

        void setMessageBus(IMessageBus *messageBus) { m_MessageBus = messageBus; }

    private:
        bool IsEnabled() const { return (m_MessageBus == nullptr) || m_MessageBus->IsMessagingEnabled(); }

    private:
        std::vector<std::reference_wrapper<MessagesTarget<T>>> m_Targets;
        IMessageBus *m_MessageBus = nullptr;
    };

    template<typename T>
    void connectTarget(MessagesTarget<T> &t,
                       IMessageBus *bus,
                       std::initializer_list<std::reference_wrapper<MessagesSource<T>>> sources) {
        for (auto s: sources) {
            s.get().addTarget(t);
            s.get().setMessageBus(bus);
        }
    }

    template<typename T>
    void connectSource(MessagesSource<T> &source,
                       IMessageBus *bus,
                       std::initializer_list<std::reference_wrapper<MessagesTarget<T>>> targets) {
        source.setMessageBus(bus);
        for (auto t: targets) {
            source.addTarget(t);
        }
    }
}

#endif // CHANGESLISTENER_H
