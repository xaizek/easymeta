/*
 * This file is a part of EasyMeta - cross-platform application
 * for basic metadata editing in photos, vectors and videos
 * Copyright (C) 2020-2021 Taras Kushnir <tk.dev@mailbox.org>
 */

#ifndef FLAGS
#define FLAGS

#include <type_traits>

#include <QMetaType>

#include "Common/types.h"  // IWYU pragma: keep

#if defined(Q_OS_WIN32) && defined(Q_PROCESSOR_X86_32) && (_MSC_VER == 1800)
#define XPIKS_TYPED_ENUMS_WORKAROUND
#endif

namespace Common {
#if !defined(XPIKS_TYPED_ENUMS_WORKAROUND)
    // visual studio 2013 bug

    template<typename FlagType>
    struct enable_bitmask_operators {
        static constexpr bool enable=false;
    };

    template<typename FlagType>
    typename std::enable_if<enable_bitmask_operators<FlagType>::enable, FlagType>::type
    operator|(FlagType a, FlagType b) {
        typedef typename std::underlying_type<FlagType>::type underlying;
        return static_cast<FlagType>(static_cast<underlying>(a) | static_cast<underlying>(b));
    }

    template<typename FlagType>
    typename std::enable_if<enable_bitmask_operators<FlagType>::enable, FlagType>::type
    operator|(FlagType a, typename std::underlying_type<FlagType>::type b) {
        typedef typename std::underlying_type<FlagType>::type underlying;
        return static_cast<FlagType>(static_cast<underlying>(a) | b);
    }

    template<typename FlagType>
    typename std::enable_if<enable_bitmask_operators<FlagType>::enable, typename std::underlying_type<FlagType>::type>::type
    operator~(FlagType a) {
        return ~(static_cast<typename std::underlying_type<FlagType>::type>(a));
    }

    template<typename FlagType>
    typename std::enable_if<enable_bitmask_operators<FlagType>::enable, FlagType>::type
    operator&(FlagType a, FlagType b) {
        typedef typename std::underlying_type<FlagType>::type underlying;
        return static_cast<FlagType>(static_cast<underlying>(a) & static_cast<underlying>(b));
    }

    template<typename FlagType>
    typename std::enable_if<enable_bitmask_operators<FlagType>::enable, FlagType>::type
    operator&(FlagType a, typename std::underlying_type<FlagType>::type b) {
        typedef typename std::underlying_type<FlagType>::type underlying;
        return static_cast<FlagType>(static_cast<underlying>(a) & b);
    }
#endif

    enum struct MetadataFlags: flag_t {
        None           = 0,
        UseTitle       = 1 << 0,
        UseDescription = 1 << 1,
        UseKeywords    = 1 << 2,
        UseCopyright   = 1 << 3,
    };

#if !defined(XPIKS_TYPED_ENUMS_WORKAROUND)
    // visual studio 2013 bug

    template<>
    struct enable_bitmask_operators<MetadataFlags> {
        static constexpr bool enable = true;
    };
#else
#define ENUM_OR(ENUM_TYPE) inline ENUM_TYPE operator | (ENUM_TYPE a, ENUM_TYPE b) { \
    using T = std::underlying_type_t <ENUM_TYPE>; \
    return (ENUM_TYPE)(static_cast<T>(a) | static_cast<T>(b)); \
}

#define ENUM_AND(ENUM_TYPE) inline ENUM_TYPE operator & (ENUM_TYPE a, ENUM_TYPE b) { \
    using T = std::underlying_type_t <ENUM_TYPE>; \
    return (ENUM_TYPE)(static_cast<T>(a) & static_cast<T>(b)); \
    } \
    inline ENUM_TYPE operator & (ENUM_TYPE a, std::underlying_type_t <ENUM_TYPE> b) { \
        using T = std::underlying_type_t <ENUM_TYPE>; \
        return (ENUM_TYPE)(static_cast<T>(a) & b); \
    }

#define ENUM_NOT(ENUM_TYPE) inline std::underlying_type_t <ENUM_TYPE> operator ~ (ENUM_TYPE a) { \
    using T = std::underlying_type_t <ENUM_TYPE>; \
    return ~(static_cast<T>(a)); \
}

    ENUM_OR(MetadataFlags)

    ENUM_AND(MetadataFlags)

    ENUM_NOT(MetadataFlags)
#endif

    // --------------------------------------------

    template<typename FlagValue, typename FlagType>
    constexpr bool HasFlag(FlagValue value, FlagType flag) {
        return (value & flag) == flag;
    }

    template<typename FlagValue, typename FlagType>
    void SetFlag(FlagValue &value, FlagType flag) {
        value = value | flag;
    }

    template<typename FlagValue, typename FlagType>
    void UnsetFlag(FlagValue &value, FlagType flag) {
        // Xpiks should not have any tricky operations
        // that might require unsettings some weird flags first
        //Q_ASSERT(HasFlag(value, flag));
        value = value & ~(flag);
    }

    template<typename FlagValue, typename FlagType>
    void ApplyFlag(FlagValue &value, bool applySwitch, FlagType flag) {
        if (applySwitch) {
            SetFlag(value, flag);
        } else {
            UnsetFlag(value, flag);
        }
    }
}

#endif // FLAGS
