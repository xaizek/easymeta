/*
 * This file is a part of EasyMeta - cross-platform application
 * for basic metadata editing in photos, vectors and videos
 * Copyright (C) 2020-2021 Taras Kushnir <tk.dev@mailbox.org>
 */

#ifndef LOGGING_H
#define LOGGING_H

#include <QDebug>  // IWYU pragma: keep
#include <QtGlobal>

#if (QT_VERSION <= QT_VERSION_CHECK(5, 4, 2))
#define qInfo qDebug
#endif

#define LOG_DEBUG qDebug()
#define LOG_INFO qInfo()

#ifdef QT_DEBUG
#define LOG_FOR_DEBUG qDebug()
#else
#define LOG_FOR_DEBUG if (1) {} else qDebug()
#endif

#ifdef CORE_TESTS
#define LOG_CORE_TESTS qDebug()
#else
#define LOG_CORE_TESTS if (1) {} else qDebug()
#endif

#if defined(VERBOSE_LOGGING)
#define LOG_VERBOSE qDebug()
#else
#define LOG_VERBOSE if (1) {} else qDebug()
#endif

#if defined(QT_DEBUG) || defined(VERBOSE_LOGGING)
#define LOG_VERBOSE_OR_DEBUG qDebug()
#else
#define LOG_VERBOSE_OR_DEBUG if (1) {} else qDebug()
#endif

#if defined(FAKE_WARNINGS)
#define LOG_WARNING qInfo() << "FAKE_WARNING"
#else
#define LOG_WARNING qWarning()
#endif

#define REPORT_ME "[rpt_me]"
#define PII_START "pIi["
#define PII_END "]Iip"
#define PII_PLACEHOLDER "[pii]"
#define PII(x) PII_START << (x) << PII_END

#if defined(INTEGRATION_TESTS) || defined(CORE_TESTS) || defined(UI_TESTS)
#define LOG_FOR_TESTS qDebug()
#else
#define LOG_FOR_TESTS if (1) {} else qDebug()
#endif

#endif // LOGGING_H
