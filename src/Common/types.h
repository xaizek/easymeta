/*
 * This file is a part of EasyMeta - cross-platform application
 * for basic metadata editing in photos, vectors and videos
 * Copyright (C) 2020-2021 Taras Kushnir <tk.dev@mailbox.org>
 */

#ifndef XPIKS_CORELIB_TYPES_H
#define XPIKS_CORELIB_TYPES_H

#include <cstdint>
#include <utility>

namespace Common {
    typedef uint32_t flag_t;

    template <typename T, int Tag=0>
    class NamedType
    {
    public:
        typedef T value_type;

    public:
        NamedType() {}
        NamedType(T const &value) : m_Value(value) {}
        NamedType(T &&value) : m_Value(std::move(value)) {}
        NamedType(const NamedType &other): m_Value(other.m_Value) {}
        T& get() { return m_Value; }
        T const& get() const { return m_Value; }
        bool operator==(NamedType<T> const &t) const {
            return m_Value == t.m_Value;
        }
        bool operator!=(NamedType<T> const &t) const {
            return m_Value != t.m_Value;
        }

    private:
        T m_Value;
    };

    using ID_t = NamedType<uint32_t>;
}

#endif // XPIKS_CORELIB_TYPES_H
