#include <iostream>

#include <QCoreApplication>
#include <QDateTime>
#include <QDir>
#include <QJsonDocument>
#include <QJsonObject>
#include <QObject>
#include <QString>
#include <QStringList>
#include <QThread>

#include "Helpers/exiv2iohelpers.h"
#include "Helpers/logger.h"
#include "Helpers/loggingworker.h"
#include "metadataworker.h"
#include "version.h"

static void responseAvailable(const QJsonDocument &response) {
    std::cout << response.toJson(QJsonDocument::Compact).toStdString() << std::endl;
}

static void queueIsEmpty() {
    // std::endl should flush as well, but just to be on the safe side
    std::cout << std::flush;
}

static Helpers::LoggingWorker *startLogger() {
    Helpers::LoggingWorker *loggingWorker = nullptr;

    Helpers::Logger &logger = Helpers::Logger::getInstance();
    QString logDirPath = QString::fromUtf8(qgetenv("LOGS_DIR"));
    logger.setMemoryOnly(logDirPath.isEmpty());

    if (!logDirPath.isEmpty()) {
        QDir logsDir(logDirPath);
        if (logsDir.exists()) {
            QString time = QDateTime::currentDateTimeUtc().toString("ddMMyyyy-hhmmss-zzz");
            QString logFilename = QString("easymeta-%1.log").arg(time);
            QString logFilePath = logsDir.filePath(logFilename);
            logger.setLogFilePath(logFilePath);

            loggingWorker = new Helpers::LoggingWorker();
            QThread *loggingThread = new QThread();
            loggingWorker->moveToThread(loggingThread);

            QObject::connect(loggingThread, &QThread::started, loggingWorker, &Helpers::LoggingWorker::process);
            QObject::connect(loggingWorker, &Helpers::LoggingWorker::stopped, loggingThread, &QThread::quit);

            QObject::connect(loggingWorker, &Helpers::LoggingWorker::stopped, loggingWorker, &Helpers::LoggingWorker::deleteLater);
            QObject::connect(loggingThread, &QThread::finished, loggingThread, &QThread::deleteLater);

            loggingThread->start(QThread::LowPriority);
        }
    }

    return loggingWorker;
}

int main(int argc, char *argv[]) {
    QCoreApplication app(argc, argv);

    QStringList args = app.arguments();
    if (args.contains("-v") || args.contains("--version")) {
        std::cout << "Easy meta: " << Version::GIT_COMMIT_SHA1 << '\n';
        return 0;
    }

    Helpers::LoggingWorker *loggingWorker = startLogger();

    Exiv2InitHelper exiv2Init;
    Q_UNUSED(exiv2Init);

    MetadataWorker worker;
    QObject::connect(&worker, &MetadataWorker::responseAvailable, &responseAvailable);
    QObject::connect(&worker, &MetadataWorker::queueIsEmpty, &queueIsEmpty);
    LOG_DEBUG << "Starting worker...";
    worker.start();
    // don't send requests until the worker is ready to serve them
    worker.waitIdle();

    LOG_DEBUG << "Starting reading from STDIN";
    for (std::string request; std::getline(std::cin, request); ) {
        auto document = QJsonDocument::fromJson(QByteArray::fromStdString(request));
        if (document.isObject()) {
            worker.submitItem(std::make_shared<QJsonObject>(document.object()));
        } else {
            LOG_DEBUG << "Skipping invalid request";
        }
    }

    LOG_DEBUG << "Finished reading from STDIN";

    if (loggingWorker != nullptr) {
        loggingWorker->cancel();
    }

    // give some time to process whatever is in the queue
    worker.waitForJobs();
    worker.stopWorking(true/*immediately*/);

    LOG_DEBUG << "Wating for worker to finish";
    worker.wait();

    return 0;
}
