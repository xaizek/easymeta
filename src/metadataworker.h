/*
 * This file is a part of EasyMeta - cross-platform application
 * for basic metadata editing in photos, vectors and videos
 * Copyright (C) 2020-2021 Taras Kushnir <tk.dev@mailbox.org>
 */

#ifndef METADATAWORKER_H
#define METADATAWORKER_H

#include <cstdint>
#include <memory>
#include <vector>

#include <Common/itemprocessingworker.h>

#include <QJsonDocument>
#include <QJsonObject>
#include <QString>
#include <QThread>

class MetadataWorker:
        public QThread,
        public Common::ItemProcessingWorker<QJsonObject, QJsonDocument>
{
    Q_OBJECT

public:
    void waitForJobs();

signals:
    void responseAvailable(const QJsonDocument &response);
    void queueIsEmpty();

    // QThread interface
protected:
    void run() override {
        doWork();
    }

    // Common::ItemProcessingWorker interface
protected:
    virtual std::shared_ptr<QJsonDocument> processWorkItem(WorkItem &workItem) override;
    virtual void onResultsAvailable(std::vector<WorkResult> &results) override;
    virtual void onQueueIsEmpty() override;

private:
    void handleRead(const QJsonObject &request, QJsonDocument &response);
    void handleWrite(const QJsonObject &request, QJsonDocument &response);
    void handleWipe(const QJsonObject &request, QJsonDocument &response);
    void handleRestore(const QJsonObject &request, QJsonDocument &response);
};

#endif // METADATAWORKER_H
