/*
 * This file is a part of EasyMeta - cross-platform application
 * for basic metadata editing in photos, vectors and videos
 * Copyright (C) 2020-2021 Taras Kushnir <tk.dev@mailbox.org>
 */

#ifndef VERSION_H
#define VERSION_H

#include <string>

struct Version {
    static const std::string GIT_COMMIT_SHA1;
};

#endif // VERSION_H
