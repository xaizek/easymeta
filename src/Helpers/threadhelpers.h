/*
 * This file is a part of EasyMeta - cross-platform application
 * for basic metadata editing in photos, vectors and videos
 * Copyright (C) 2020-2021 Taras Kushnir <tk.dev@mailbox.org>
 */

#ifndef THREADHELPERS_H
#define THREADHELPERS_H

#include <atomic>

#include <QMutex>
#include <QWaitCondition>

namespace Helpers {
    class ManualResetEvent
    {
    public:
        ManualResetEvent();

    public:
        void set();
        void reset();
        void waitOne();

    private:
        QWaitCondition m_WaitCondition;
        QMutex m_Mutex;
        std::atomic_bool m_Flag;
    };
}

#endif // THREADHELPERS_H
