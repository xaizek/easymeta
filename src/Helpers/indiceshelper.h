/*
 * This file is a part of EasyMeta - cross-platform application
 * for basic metadata editing in photos, vectors and videos
 * Copyright (C) 2020-2021 Taras Kushnir <tk.dev@mailbox.org>
 */
#ifndef INDICESHELPER_H
#define INDICESHELPER_H

#include <utility>
#include <vector>

template <typename T> class QVector;

namespace Helpers {
    typedef std::vector<std::pair<int, int> > RangesVector;

    template<class T>
    int splitIntoChunks(const QVector<T> &items, int chunksCount, QVector<QVector<T> > &chunks);
    RangesVector unionRanges(RangesVector &ranges);
}

#endif // INDICESHELPER_H
