/*
 * This file is a part of Xpiks - cross platform application for
 * keywording and uploading images for microstocks
 * Copyright (C) 2020-2021 Taras Kushnir <tk.dev@mailbox.org>
 */

#include "threadhelpers.h"

#include <QMutexLocker>
#include <QtGlobal>

namespace Helpers {
    ManualResetEvent::ManualResetEvent():
        m_Flag(false)
    {
    }

    void ManualResetEvent::set() {
        QMutexLocker locker(&m_Mutex);
        Q_UNUSED(locker);
        if (!m_Flag) {
            m_Flag = true;
            m_WaitCondition.wakeAll();
        }
    }

    void ManualResetEvent::reset() {
        QMutexLocker locker(&m_Mutex);
        Q_UNUSED(locker);
        m_Flag = false;
    }

    void ManualResetEvent::waitOne() {
        QMutexLocker locker(&m_Mutex);
        Q_UNUSED(locker);

        while (!m_Flag) {
            m_WaitCondition.wait(&m_Mutex);
        }
    }
}
