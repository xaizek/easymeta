/*
 * This file is a part of EasyMeta - cross-platform application
 * for basic metadata editing in photos, vectors and videos
 * Copyright (C) 2020-2021 Taras Kushnir <tk.dev@mailbox.org>
 */

#ifndef LOGGINGWORKER_H
#define LOGGINGWORKER_H

#include <atomic>

#include <QObject>
#include <QString>

namespace Helpers {
    class LoggingWorker : public QObject
    {
        Q_OBJECT
    public:
        explicit LoggingWorker(QObject *parent = nullptr);
        virtual ~LoggingWorker() {}

    signals:
        void stopped();

    public slots:
        void process();
        void cancel();

    private:
        std::atomic_bool m_Cancel;
    };
}

#endif // LOGGINGWORKER_H
