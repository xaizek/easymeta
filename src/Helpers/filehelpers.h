/*
 * This file is a part of EasyMeta - cross-platform application
 * for basic metadata editing in photos, vectors and videos
 * Copyright (C) 2020-2021 Taras Kushnir <tk.dev@mailbox.org>
 */

#ifndef FILEHELPERS_H
#define FILEHELPERS_H

#include <QString>

namespace Helpers {
    bool removeFile(const QString &fullpath, int retries);
}

#endif // FILEHELPERS_H
