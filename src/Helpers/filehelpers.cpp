/*
 * This file is a part of EasyMeta - cross-platform application
 * for basic metadata editing in photos, vectors and videos
 * Copyright (C) 2020-2021 Taras Kushnir <tk.dev@mailbox.org>
 */

#include "filehelpers.h"

#include <QFile>
#include <QString>
#include <QThread>

#include "Common/logging.h"

namespace Helpers {
    const int REMOVE_FILE_RETRY_INTERVAL = 200;

    bool removeFile(const QString &fullpath, int retries) {
            LOG_DEBUG << "Attempting to remove file" << fullpath;
            bool removed = false;

            QFile file(fullpath);
            int attempt = 0;
            do {
                if (file.remove()) {
                    removed = true;
                    break;
                }
                LOG_WARNING << "Removing" << fullpath << "failed, error:" << file.errorString();
                QThread::usleep(REMOVE_FILE_RETRY_INTERVAL);
            } while (attempt++ < retries);

            return removed;
        }
}
