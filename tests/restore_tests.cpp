#include "restore_tests.h"

#include <QFileInfo>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QString>
#include <QStringList>

#include "Helpers/constants.h"

#include "testhelpers.h"

void RestoreTests::init() {
    m_EasyMeta.start("../src/easymeta", QStringList());
    QVERIFY(m_EasyMeta.waitForStarted(TIMEOUT_MS));
}

void RestoreTests::cleanup() {
    m_EasyMeta.terminate();
    if (!m_EasyMeta.waitForFinished(TIMEOUT_MS)) {
        m_EasyMeta.kill();
        QVERIFY(m_EasyMeta.waitForFinished(TIMEOUT_MS));
    }
}

static void singleFileTest(QProcess &easyMeta,
                           const QString &filepath,
                           const QString &title,
                           const QString &description,
                           const QStringList &keywords) {
    QJsonObject response;
    QString fileCopy = setupFilePathForTest(filepath);
    QString backup = fileCopy + "_original";

    QVERIFY(sendSimpleRequest(easyMeta, WIPE_TYPE, { fileCopy }, true));
    receiveResponse(easyMeta, response);
    QVERIFY(!response.isEmpty());

    QVERIFY(QFileInfo::exists(backup));

    QVERIFY(sendSimpleRequest(easyMeta, RESTORE_TYPE, { fileCopy }));
    receiveResponse(easyMeta, response);
    QVERIFY(!response.isEmpty());

    QVERIFY(!QFileInfo::exists(backup));

    QVERIFY(sendSimpleRequest(easyMeta, READ_TYPE, { fileCopy }));
    receiveResponse(easyMeta, response);
    QVERIFY(!response.isEmpty());

    QJsonArray filesArray = response[FILES_KEY].toArray();
    QCOMPARE(filesArray.size(), 1);
    QVERIFY(filesArray[0].isObject());
    QJsonObject file = filesArray[0].toObject();
    QCOMPARE(file[TITLE_KEY].toString(), title);
    QCOMPARE(file[DESCRIPTION_KEY].toString(), description);
    QCOMPARE(asString(file[KEYWORDS_KEY].toArray()), asString(keywords));
}

void RestoreTests::jpgFileTest() {
    QStringList keywords = {
        "abstract", "art", "black", "blue", "creative", "dark", "decor",
        "decoration", "decorative", "design", "dot", "drops", "elegance",
        "element", "geometric", "interior", "light", "modern", "old", "ornate",
        "paper", "pattern", "purple", "retro", "seamless", "style", "textile",
        "texture", "vector", "wall", "wallpaper"
    };

    singleFileTest(m_EasyMeta,
                   "tests/data/026.jpg",
                   "background droplets",
                   "background droplets vector illustration with textures",
                   keywords);
}

void RestoreTests::badFileEntryTest() {
    QVERIFY(sendSimpleRequest(m_EasyMeta, RESTORE_TYPE, { "" }));
    checkFileErrorResponse(m_EasyMeta, "File name is missing or empty");
}

void RestoreTests::missingFileTest() {
    QVERIFY(sendSimpleRequest(m_EasyMeta, RESTORE_TYPE, { "no/such/file" }));
    checkFileErrorResponse(m_EasyMeta, "File doesn't exist");
}

void RestoreTests::multipleFilesTest() {
    QStringList files = {
        setupFilePathForTest("tests/data/026.jpg"),
        setupFilePathForTest("tests/data/027.jpg")
    };

    QJsonObject response;

    QVERIFY(sendSimpleRequest(m_EasyMeta, WIPE_TYPE, files));
    receiveResponse(m_EasyMeta, response);
    QVERIFY(!response.isEmpty());

    QVERIFY(sendSimpleRequest(m_EasyMeta, RESTORE_TYPE, files));
    receiveResponse(m_EasyMeta, response);
    QVERIFY(!response.isEmpty());

    QVERIFY(response[FILES_KEY].isArray());
    QJsonArray filesArray = response[FILES_KEY].toArray();
    QCOMPARE(filesArray.size(), 2);
    for (const QJsonValue file: filesArray) {
        QVERIFY(file.isObject());
        QJsonObject fileObject = file.toObject();
        QVERIFY(!fileObject.contains(ERROR_KEY));

        QString filepath = fileObject[SOURCE_FILE_KEY].toString();
        QVERIFY(files.contains(filepath));

        QVERIFY2(!QFileInfo::exists(filepath + "_original"),
                 (filepath + "_original").toStdString().c_str());
    }
}
