/*
 * This file is a part of EasyMeta - cross-platform application
 * for basic metadata editing in photos, vectors and videos
 * Copyright (C) 2020-2021 Taras Kushnir <tk.dev@mailbox.org>
 */

#include "write_tests.h"

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QProcess>
#include <QString>
#include <QStringList>

#include "Helpers/constants.h"

#include "testhelpers.h"

void WriteTests::init() {
    m_EasyMeta.start("../src/easymeta", QStringList());
    QVERIFY(m_EasyMeta.waitForStarted(TIMEOUT_MS));
}

void WriteTests::cleanup() {
    m_EasyMeta.terminate();
    if (!m_EasyMeta.waitForFinished(TIMEOUT_MS)) {
        m_EasyMeta.kill();
        QVERIFY(m_EasyMeta.waitForFinished(TIMEOUT_MS));
    }
}

static void sendWriteRequest(QProcess &easyMeta,
                             const QStringList &filepaths,
                             const QString &title,
                             const QString &description,
                             const QString &copyright,
                             const QStringList &keywords,
                             bool withBackups = false) {
    QJsonObject request;
    request[ID_KEY] = "testid";
    request[TYPE_KEY] = WRITE_TYPE;
    request[BACKUPS_KEY] = withBackups;

    QJsonArray filesArray;
    for (const QString &filepath: filepaths) {
        QJsonObject fileObject;
        fileObject[SOURCE_FILE_KEY] = filepath;
        fileObject[TITLE_KEY] = title;
        fileObject[DESCRIPTION_KEY] = description;
        fileObject[COPYRIGHT_KEY] = copyright;
        fileObject[KEYWORDS_KEY] = QJsonArray::fromStringList(keywords);
        filesArray.append(fileObject);
    }
    request[FILES_KEY] = filesArray;

    QJsonDocument replyDocument;
    replyDocument.setObject(request);

    easyMeta.write(replyDocument.toJson(QJsonDocument::Compact));
    easyMeta.write("\n");
    QVERIFY(easyMeta.waitForBytesWritten(TIMEOUT_MS));
}

static void verifyMetadata(const QString &filepath,
                           const QString &title,
                           const QString &description,
                           const QStringList &keywords) {
    QJsonDocument exifResponseDocument;
    readWithExiftool(filepath, exifResponseDocument);
    QVERIFY(!exifResponseDocument.isNull());
    QJsonObject metadata = exifResponseDocument.array()[0].toObject();

    QCOMPARE(metadata["Title"].toString(), title);
    QCOMPARE(metadata["ObjectName"].toString(), title);

    QCOMPARE(metadata["Caption-Abstract"].toString(), description);
    QCOMPARE(metadata["Description"].toString(), description);
    QCOMPARE(metadata["ImageDescription"].toString(), description);

    QCOMPARE(metadata["Keywords"], QJsonArray::fromStringList(keywords));
    QCOMPARE(metadata["Subject"].toString(), keywords.join(", "));
}

void WriteTests::badFileEntryTest() {
    sendWriteRequest(m_EasyMeta, { "" }, "title", "descr", "(c)", { "kw1" });
    checkFileErrorResponse(m_EasyMeta, "File name is missing or empty");
}

void WriteTests::missingFileTest() {
    sendWriteRequest(m_EasyMeta, { "no/such/path" }, "title", "descr", "(c)", { "kw1" });
    checkFileErrorResponse(m_EasyMeta, "File doesn't exist");
}

void WriteTests::unsupportedFileTest() {
    QString filepath = setupFilePathForTest("tests/data/vector-icon.svg");
    sendWriteRequest(m_EasyMeta, { filepath }, "title", "descr", "(c)", { "kw1" });
    checkFileErrorResponse(m_EasyMeta, "Couldn't update metadata");
}

static void singleFileTest(QProcess &easyMeta,
                           const QString &filepath,
                           const QString &title,
                           const QString &description,
                           const QString &copyright,
                           const QStringList &keywords,
                           bool withBackups) {
    QString fileCopy = setupFilePathForTest(filepath);
    sendWriteRequest(easyMeta, { fileCopy }, title, description, copyright, keywords, withBackups);

    QJsonObject response;
    receiveResponse(easyMeta, response);
    QVERIFY(!response.isEmpty());
    QCOMPARE(response[ID_KEY], "testid");
    QJsonArray filesArray = response[FILES_KEY].toArray();
    QCOMPARE(filesArray.size(), 1);
    QJsonObject fileObject = filesArray[0].toObject();
    QCOMPARE(fileObject[SOURCE_FILE_KEY], fileCopy);
    QVERIFY(!fileObject.contains(ERROR_KEY));

    if (withBackups) {
        QVERIFY(QFileInfo::exists(fileCopy + "_original"));
    }

    verifyMetadata(fileCopy, title, description, keywords);
}

void WriteTests::jpgFileTest() {
    QString title = "jpgFileTest title";
    QString description = "jpgFileTest description";
    QString copyright = "jpgFileTest copyright";
    QStringList keywords = { "jpgFileTest", "key", "words" };

    singleFileTest(m_EasyMeta, { "tests/data/027.jpg" }, title, description, copyright, keywords, false/*withBackups*/);
    singleFileTest(m_EasyMeta, { "tests/data/027.jpg" }, title, description, copyright, keywords, true/*withBackups*/);
}

void WriteTests::multipleFilesTest() {
    QStringList filepaths = {
        setupFilePathForTest("tests/data/026.jpg"),
        setupFilePathForTest("tests/data/027.jpg")
    };
    QString title = "multipleFilesTest title";
    QString description = "multipleFilesTest description";
    QString copyright = "multipleFilesTest copyright ";
    QStringList keywords = { "multipleFilesTest", "key", "words" };

    sendWriteRequest(m_EasyMeta, filepaths, title, description, copyright, keywords);

    QJsonObject response;
    receiveResponse(m_EasyMeta, response);
    QVERIFY(!response.isEmpty());
    QCOMPARE(response[ID_KEY], "testid");
    QJsonArray filesArray = response[FILES_KEY].toArray();
    QCOMPARE(filesArray.size(), 2);
    QJsonObject fileObject = filesArray[0].toObject();
    QVERIFY(filepaths.contains(fileObject[SOURCE_FILE_KEY].toString()));
    QVERIFY(!fileObject.contains(ERROR_KEY));
    fileObject = filesArray[1].toObject();
    QVERIFY(filepaths.contains(fileObject[SOURCE_FILE_KEY].toString()));
    QVERIFY(!fileObject.contains(ERROR_KEY));

    verifyMetadata(filepaths[0], title, description, keywords);
    verifyMetadata(filepaths[1], title, description, keywords);
}

static void roundTripTest(QProcess &easyMeta,
                          const QString &filepath,
                          const QString &title,
                          const QString &description,
                          const QString &copyright,
                          const QStringList &keywords) {
    sendWriteRequest(easyMeta, { filepath }, title, description, copyright, keywords);

    QJsonObject readResponse;
    receiveResponse(easyMeta, readResponse);
    QVERIFY(!readResponse.isEmpty());

    QVERIFY(sendSimpleRequest(easyMeta, READ_TYPE, { filepath }));
    QJsonObject writeResponse;
    receiveResponse(easyMeta, writeResponse);
    QVERIFY(!writeResponse.isEmpty());

    QVERIFY(writeResponse[FILES_KEY].isArray());
    QJsonArray filesArray = writeResponse[FILES_KEY].toArray();
    QCOMPARE(filesArray.size(), 1);

    QVERIFY(filesArray[0].isObject());
    QJsonObject file = filesArray[0].toObject();
    QCOMPARE(file[TITLE_KEY], title);
    QCOMPARE(file[DESCRIPTION_KEY], description);
    QCOMPARE(asString(file[KEYWORDS_KEY].toArray()), asString(keywords));
}

void WriteTests::asciiRoundTripTest() {
    QString filepath = setupFilePathForTest("tests/data/clear.jpg");
    QString title = "asciiRoundTripTest title";
    QString description = "asciiRoundTripTest description";
    QString copyright = "asciiRoundTripTest copyright ";
    QStringList keywords = { "ascii", "round-trip", "test", "tags" };

    // this creates metadata for the first time
    roundTripTest(m_EasyMeta, filepath, title, description, copyright, keywords);
    // this updates existing metadata
    roundTripTest(m_EasyMeta, filepath, title, description, copyright, keywords);
}

void WriteTests::unicodeRoundTripTest() {
    QString filepath = setupFilePathForTest("tests/data/clear.jpg");
    QString title = "πύργος του Άιφελ";
    QString description = "První plány stavby byly zahájeny už v roce 1878.";
    QString copyright = "Все права сохранены";
    QStringList keywords = {
        "buokšts", "sėmbuolu", "Parīžiuo", "aukštliausės", "bodīnks", "metās",
        "Е́йфелева", "ве́жа", "埃菲尔铁塔"
    };

    // this creates metadata for the first time
    roundTripTest(m_EasyMeta, filepath, title, description, copyright, keywords);
    // this updates existing metadata
    roundTripTest(m_EasyMeta, filepath, title, description, copyright, keywords);
}
