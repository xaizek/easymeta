/*
 * This file is a part of EasyMeta - cross-platform application
 * for basic metadata editing in photos, vectors and videos
 * Copyright (C) 2020-2021 Taras Kushnir <tk.dev@mailbox.org>
 */

#include "wipe_tests.h"

#include <QFileInfo>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QString>
#include <QStringList>

#include "Helpers/constants.h"

#include "testhelpers.h"

void WipeTests::init() {
    m_EasyMeta.start("../src/easymeta", QStringList());
    QVERIFY(m_EasyMeta.waitForStarted(TIMEOUT_MS));
}

void WipeTests::cleanup() {
    m_EasyMeta.terminate();
    if (!m_EasyMeta.waitForFinished(TIMEOUT_MS)) {
        m_EasyMeta.kill();
        QVERIFY(m_EasyMeta.waitForFinished(TIMEOUT_MS));
    }
}

static void singleFileTest(QProcess &easyMeta, const QString &filepath, bool withBackups) {
    QString fileCopy = setupFilePathForTest(filepath);
    QVERIFY(sendSimpleRequest(easyMeta, WIPE_TYPE, { fileCopy }, withBackups));

    QJsonObject response;
    receiveResponse(easyMeta, response);
    QVERIFY(!response.isEmpty());
    QCOMPARE(response[ID_KEY], "testid");

    QVERIFY(response[FILES_KEY].isArray());
    QJsonArray filesArray = response[FILES_KEY].toArray();
    QCOMPARE(filesArray.size(), 1);
    QVERIFY(!filesArray[0].toObject().contains(ERROR_KEY));

    if (withBackups) {
        QVERIFY(QFileInfo::exists(fileCopy + "_original"));
    }

    QJsonDocument exifResponseDocument;
    readWithExiftool(fileCopy, exifResponseDocument);
    QVERIFY(!exifResponseDocument.isNull());
    QJsonObject metadata = exifResponseDocument.array()[0].toObject();

    QCOMPARE(metadata["Title"].toString(), QString());
    QCOMPARE(metadata["ObjectName"].toString(), QString());

    QCOMPARE(metadata["Caption-Abstract"].toString(), QString());
    QCOMPARE(metadata["Description"].toString(), QString());
    QCOMPARE(metadata["ImageDescription"].toString(), QString());

    QCOMPARE(metadata["Keywords"].toArray(), QJsonArray());
    QCOMPARE(metadata["Subject"].toString(), QString());
}

void WipeTests::jpgFileTest() {
    singleFileTest(m_EasyMeta, "tests/data/026.jpg", false/*withBackups*/);
    singleFileTest(m_EasyMeta, "tests/data/026.jpg", true/*withBackups*/);
}

void WipeTests::badFileEntryTest() {
    QVERIFY(sendSimpleRequest(m_EasyMeta, WIPE_TYPE, { "" }));
    checkFileErrorResponse(m_EasyMeta, "File name is missing or empty");
}

void WipeTests::missingFileTest() {
    QVERIFY(sendSimpleRequest(m_EasyMeta, WIPE_TYPE, { "no/such/file" }));
    checkFileErrorResponse(m_EasyMeta, "File doesn't exist");
}

void WipeTests::unsupportedFileTest() {
    QStringList files = {
        findFullPathForTests("tests/data/vector-icon.svg")
    };
    QVERIFY(sendSimpleRequest(m_EasyMeta, WIPE_TYPE, files));
    checkFileErrorResponse(m_EasyMeta, "Couldn't wipe metadata");
}

void WipeTests::multipleFilesTest() {
    QStringList files = {
        setupFilePathForTest("tests/data/026.jpg"),
        setupFilePathForTest("tests/data/027.jpg")
    };
    QVERIFY(sendSimpleRequest(m_EasyMeta, WIPE_TYPE, files));

    QJsonObject response;
    receiveResponse(m_EasyMeta, response);
    QVERIFY(!response.isEmpty());
    QCOMPARE(response[ID_KEY].toString(), "testid");

    QVERIFY(response[FILES_KEY].isArray());
    QJsonArray filesArray = response[FILES_KEY].toArray();
    QCOMPARE(filesArray.size(), 2);
    for (const QJsonValue file: filesArray) {
        QVERIFY(file.isObject());
        QJsonObject fileObject = file.toObject();
        QVERIFY(!fileObject.contains(ERROR_KEY));

        QString filepath = fileObject[SOURCE_FILE_KEY].toString();
        QVERIFY(files.contains(filepath));

        QVERIFY2(QFileInfo::exists(filepath + "_original"),
                 (filepath + "_original").toStdString().c_str());
    }
}
