/*
 * This file is a part of EasyMeta - cross-platform application
 * for basic metadata editing in photos, vectors and videos
 * Copyright (C) 2020-2021 Taras Kushnir <tk.dev@mailbox.org>
 */

#include "testhelpers.h"

#include <chrono>
#include <thread>

#include <QFile>
#include <QFileInfo>
#include <QString>
#include <QStringList>
#include <QtTest>

#include "Helpers/constants.h"

const std::chrono::milliseconds SLEEP_PERIOD(100);
const int WAIT_PERIOD_COUNT = 5;

QString asString(QStringList list) {
    list.sort();
    return list.join(";");
}

QString asString(const QJsonArray &array) {
    QStringList list;
    for (const QJsonValue &value: array) {
        list << value.toString();
    }
    return asString(list);
}

static bool tryFindFullPathForTests(const QString &prefix, QString &path) {
    QFileInfo fi(prefix);
    int tries = 6;
    QStringList parents;
    while (tries--) {
        if (!fi.exists()) {
            parents.append("..");
            fi.setFile(parents.join('/') + "/" + prefix);
        } else {
            path = fi.absoluteFilePath();
            return true;
        }
    }

    return false;
}

QString findFullPathForTests(const QString &prefix) {
    QString foundPath;
    if (!tryFindFullPathForTests(prefix, foundPath)) {
        foundPath = QFileInfo(prefix).absoluteFilePath();
    }
    return foundPath;
}

static void ensureDirectoryExistsForFile(const QString &filepath) {
    QFileInfo fi(filepath);

    if (fi.exists()) { return; }

    QDir filesDir = fi.absoluteDir();
    if (!filesDir.exists()) {
        if (!filesDir.mkpath(".")) {
            qWarning() << "Failed to create a path:" << filesDir.path();
        }
    }
}

static bool copyFile(const QString &from, const QString &to) {
    bool success = false;

    QFile destination(to);
    if (destination.exists()) {
        if (!destination.remove()) {
            return success;
        }
    }

    QFile source(from);
    if (source.exists()) {
        success = source.copy(to);
    }

    return success;
}

QString setupFilePathForTest(const QString &prefix) {
    QString fullPath;
    if (tryFindFullPathForTests(prefix, fullPath)) {
        qInfo() << "Found artwork at" << fullPath;
        QString sessionPath = QDir::cleanPath("session/" + prefix);
        ensureDirectoryExistsForFile(sessionPath);
        if (copyFile(fullPath, sessionPath)) {
            qInfo() << "Copied artwork to" << sessionPath;
            return sessionPath;
        } else {
            return fullPath;
        }
    }

    return prefix;
}

void cleanupSessionDir() {
    QDir sessionDir("session");
    if (!sessionDir.removeRecursively()) {
        qWarning() << "Failed to remove session directory";
    }
}

bool sendSimpleRequest(QProcess &easyMeta, const QString &type, const QStringList &files, bool withBackups) {
    QJsonObject request;
    request[ID_KEY] = "testid";
    request[TYPE_KEY] = type;
    request[BACKUPS_KEY] = withBackups;
    request[FILES_KEY] = QJsonArray::fromStringList(files);

    QJsonDocument document;
    document.setObject(request);

    easyMeta.write(document.toJson(QJsonDocument::Compact));
    easyMeta.write("\n");
    return easyMeta.waitForBytesWritten(TIMEOUT_MS);
}

void receiveResponse(QProcess &easyMeta, QJsonObject &response) {
    QVERIFY(easyMeta.waitForReadyRead(TIMEOUT_MS));
    for (int i = 0; i < WAIT_PERIOD_COUNT && !easyMeta.canReadLine(); ++i) {
        std::this_thread::sleep_for(SLEEP_PERIOD);
    }
    QVERIFY(easyMeta.canReadLine());

    auto responseDocument = QJsonDocument::fromJson(easyMeta.readLine());
    QVERIFY(responseDocument.isObject());

    response = responseDocument.object();
}

void readWithExiftool(const QString &path, QJsonDocument &document) {
    QStringList arguments = {
        "-json", "-ignoreMinorErrors", "-e",
        "-ObjectName", "-Title",
        "-ImageDescription", "-Description", "-Caption-Abstract",
        "-Keywords", "-Subject",
        path
    };

    QProcess exiftool;
    exiftool.start("exiftool", arguments);
    QVERIFY(exiftool.waitForFinished());
    QCOMPARE(exiftool.exitCode(), 0);

    QByteArray output = exiftool.readAllStandardOutput();
    document = QJsonDocument::fromJson(output);
}

void checkFileErrorResponse(QProcess &easyMeta, const QString &error) {
    QJsonObject response;
    receiveResponse(easyMeta, response);
    QVERIFY(!response.isEmpty());
    QCOMPARE(response[ID_KEY].toString(), "testid");

    QVERIFY(response[FILES_KEY].isArray());
    QJsonArray filesArray = response[FILES_KEY].toArray();
    QCOMPARE(filesArray.size(), 1);

    QVERIFY(filesArray[0].isObject());
    QJsonObject file = filesArray[0].toObject();
    QCOMPARE(file[ERROR_KEY].toString(), error);
}
