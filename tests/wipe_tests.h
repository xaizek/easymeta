/*
 * This file is a part of EasyMeta - cross-platform application
 * for basic metadata editing in photos, vectors and videos
 * Copyright (C) 2020-2021 Taras Kushnir <tk.dev@mailbox.org>
 */

#ifndef WIPETESTS_H
#define WIPETESTS_H

#include <QObject>
#include <QProcess>
#include <QtTest>

class WipeTests: public QObject
{
    Q_OBJECT
private slots:
    void init();
    void cleanup();

    void jpgFileTest();
    void badFileEntryTest();
    void missingFileTest();
    void unsupportedFileTest();
    void multipleFilesTest();

private:
    QProcess m_EasyMeta;
};

#endif // WIPETESTS_H
