/*
 * This file is a part of EasyMeta - cross-platform application
 * for basic metadata editing in photos, vectors and videos
 * Copyright (C) 2020-2021 Taras Kushnir <tk.dev@mailbox.org>
 */

#include "read_tests.h"

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QString>
#include <QStringList>

#include "Helpers/constants.h"

#include "testhelpers.h"

void ReadTests::init() {
    m_EasyMeta.start("../src/easymeta", QStringList());
    QVERIFY(m_EasyMeta.waitForStarted(TIMEOUT_MS));
}

void ReadTests::cleanup() {
    m_EasyMeta.terminate();
    if (!m_EasyMeta.waitForFinished(TIMEOUT_MS)) {
        m_EasyMeta.kill();
        QVERIFY(m_EasyMeta.waitForFinished(TIMEOUT_MS));
    }
}

static void singleFileTest(QProcess &easyMeta,
                           const QString &filepath,
                           const QString &title,
                           const QString &description,
                           const QStringList &keywords) {
    QVERIFY(sendSimpleRequest(easyMeta, READ_TYPE, { findFullPathForTests(filepath) }));

    QJsonObject response;
    receiveResponse(easyMeta, response);
    QVERIFY(!response.isEmpty());
    QCOMPARE(response[ID_KEY], "testid");

    QVERIFY(response[FILES_KEY].isArray());
    QJsonArray filesArray = response[FILES_KEY].toArray();
    QCOMPARE(filesArray.size(), 1);

    QVERIFY(filesArray[0].isObject());
    QJsonObject file = filesArray[0].toObject();
    QCOMPARE(file[TITLE_KEY].toString(), title);
    QCOMPARE(file[DESCRIPTION_KEY].toString(), description);
    QCOMPARE(asString(file[KEYWORDS_KEY].toArray()), asString(keywords));
}

void ReadTests::jpgFileTest() {
    QStringList keywords = {
        "abstract", "art", "black", "blue", "creative", "dark", "decor",
        "decoration", "decorative", "design", "dot", "drops", "elegance",
        "element", "geometric", "interior", "light", "modern", "old", "ornate",
        "paper", "pattern", "purple", "retro", "seamless", "style", "textile",
        "texture", "vector", "wall", "wallpaper"
    };

    singleFileTest(m_EasyMeta,
                   "tests/data/026.jpg",
                   "background droplets",
                   "background droplets vector illustration with textures",
                   keywords);
}

static void checkReadFileErrorResponse(QProcess &easyMeta, const QString &error) {
    QJsonObject response;
    receiveResponse(easyMeta, response);
    QVERIFY(!response.isEmpty());
    QCOMPARE(response[ID_KEY].toString(), "testid");

    QVERIFY(response[FILES_KEY].isArray());
    QJsonArray filesArray = response[FILES_KEY].toArray();
    QCOMPARE(filesArray.size(), 1);

    QVERIFY(filesArray[0].isObject());
    QJsonObject file = filesArray[0].toObject();
    QVERIFY(!file.contains(TITLE_KEY));
    QVERIFY(!file.contains(DESCRIPTION_KEY));
    QVERIFY(!file.contains(KEYWORDS_KEY));
    QCOMPARE(file[ERROR_KEY].toString(), error);
}

void ReadTests::badFileEntryTest() {
    QVERIFY(sendSimpleRequest(m_EasyMeta, READ_TYPE, { "" }));
    checkReadFileErrorResponse(m_EasyMeta, "File name is missing or empty");
}

void ReadTests::missingFileTest() {
    QVERIFY(sendSimpleRequest(m_EasyMeta, READ_TYPE, { "no/such/file" }));
    checkReadFileErrorResponse(m_EasyMeta, "File doesn't exist");
}

void ReadTests::unsupportedFileTest() {
    QStringList files = {
        findFullPathForTests("tests/data/vector-icon.svg")
    };
    QVERIFY(sendSimpleRequest(m_EasyMeta, READ_TYPE, files));
    checkReadFileErrorResponse(m_EasyMeta, "Couldn't read metadata");
}

void ReadTests::multipleFilesTest() {
    QStringList files = {
        findFullPathForTests("tests/data/026.jpg"),
        findFullPathForTests("tests/data/027.jpg")
    };
    QVERIFY(sendSimpleRequest(m_EasyMeta, READ_TYPE, files));

    QJsonObject response;
    receiveResponse(m_EasyMeta, response);
    QVERIFY(!response.isEmpty());
    QCOMPARE(response[ID_KEY].toString(), "testid");

    QVERIFY(response[FILES_KEY].isArray());
    QJsonArray filesArray = response[FILES_KEY].toArray();
    QCOMPARE(filesArray.size(), 2);
    for (const QJsonValue file: filesArray) {
        QVERIFY(file.isObject());
        QJsonObject fileObject = file.toObject();
        QVERIFY(files.contains(fileObject[SOURCE_FILE_KEY].toString()));
    }
}
