/*
 * This file is a part of EasyMeta - cross-platform application
 * for basic metadata editing in photos, vectors and videos
 * Copyright (C) 2020-2021 Taras Kushnir <tk.dev@mailbox.org>
 */

#include <QCoreApplication>
#include <QtTest>

#include "generic_tests.h"
#include "read_tests.h"
#include "restore_tests.h"
#include "testhelpers.h"
#include "wipe_tests.h"
#include "write_tests.h"

#define QTEST_CLASS(TestObject, result) \
    { \
        TestObject testObject; \
        cleanupSessionDir(); \
        result += QTest::qExec(&testObject, argc, argv); \
    }

int main(int argc, char *argv[]) {
    qSetMessagePattern("T#%{threadid} %{function} - %{message}");

    QCoreApplication app(argc, argv);
    Q_UNUSED(app);

    int result = 0;

    QTEST_CLASS(GenericTests, result);
    QTEST_CLASS(ReadTests, result);
    QTEST_CLASS(RestoreTests, result);
    QTEST_CLASS(WipeTests, result);
    QTEST_CLASS(WriteTests, result);

    return result;
}
