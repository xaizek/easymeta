#ifndef RESTORETESTS_H
#define RESTORETESTS_H

#include <QObject>
#include <QProcess>
#include <QtTest>

class RestoreTests: public QObject
{
    Q_OBJECT
private slots:
    void init();
    void cleanup();

    void jpgFileTest();
    void badFileEntryTest();
    void missingFileTest();
    void multipleFilesTest();

private:
    QProcess m_EasyMeta;
};

#endif // RESTORETESTS_H
