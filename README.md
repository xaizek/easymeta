# Easy meta

A standalone executable for reading and writing some metadata in photos, vectors and videos

# Sample interaction

## Reading metadata

Input (newlines must be stripped):

    {
        "id": "1",
        "type": "read",
        "files": [
            "/home/xpiks/repos/xpiks/src/xpiks-tests/images-for-tests/mixed/cat.jpg"
        ]
    }

Output (formatted):

    {
        "id": "1",
        "files": [
            {
                "Description": "grumpy Exotic tortoiseshell cat portrait",
                "Keywords": [],
                "SourceFile": "/home/xpiks/repos/xpiks/src/xpiks-tests/images-for-tests/mixed/cat.jpg",
                "Title": "178110810"
            }
        ]
    }

## Wiping metadata

Input (newlines must be stripped):

    {
        "id": "2",
        "type": "wipe",
        "backups": true,
        "files": [
            "/home/xpiks/repos/easymeta/cat.jpg"
        ]
    }

Output (formatted):

    {
        "id": "2",
        "files": [
            {
                "SourceFile": "/home/xpiks/repos/easymeta/cat.jpg"
            }
        ]
    }

## Writing metadata

Input (newlines must be stripped):

    {
        "id": "3",
        "type": "write",
        "backups": false,
        "files": [
            {
                "SourceFile": "/home/xpiks/repos/easymeta/cat.jpg",
                "Description": "new new description",
                "Title": "new title",
                "Keywords": [ "KWD1", "KWD2" ]
            }
        ]
    }

Output (formatted):

    {
        "id": "3",
        "files": [
            {
                "SourceFile": "/home/xpiks/repos/easymeta/cat.jpg"
            }
        ]
    }

## Restoring from backups

Input (newlines must be stripped):

    {
        "id": "4",
        "type": "restore",
        "files": [
            "/home/xpiks/repos/easymeta/cat.jpg"
        ]
    }

Output (formatted):

    {
        "id": "4",
        "files": [
            {
                "SourceFile": "/home/xpiks/repos/easymeta/cat.jpg",
                "Error": "Couldn't find backup copy"
            }
        ]
    }
